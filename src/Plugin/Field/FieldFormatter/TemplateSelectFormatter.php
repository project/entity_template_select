<?php

namespace Drupal\entity_template_select\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'template_select_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "template_select_formatter",
 *   label = @Translation("Template select formatter"),
 *   field_types = {
 *     "template_select_type"
 *   }
 * )
 */
class TemplateSelectFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return [];
  }

}
