<?php

namespace Drupal\entity_template_select;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions of the entity template select module.
 */
class EntityTemplateSelectPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a new EntityTemplateSelectPermissions instance.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity.manager'));
  }

  /**
   * Returns an array of filter permissions.
   *
   * @return array
   */
  public function permissions() {
    $permissions = [];
    $cfEntities = \Drupal::config('entity_template_select.entity_template_setting')->get('entities');
    $entities = \Drupal::entityTypeManager()->getDefinitions();
    if (!empty($cfEntities)) {
      foreach ($cfEntities as $entity_type) {
        if (empty($entity_type)) {
          continue;
        }

        $permissions["allow users to pick $entity_type templates"] = [
          'title' => t('%entity_type: Enable Entity Template Select', [
            '%entity_type' => $entities[$entity_type]->get('label'),
          ]
          ),
        ];
      }
    }
    return $permissions;
  }

}
