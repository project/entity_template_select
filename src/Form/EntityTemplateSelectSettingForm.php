<?php

namespace Drupal\entity_template_select\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class EntityTemplateSelectSettingForm.
 */
class EntityTemplateSelectSettingForm extends ConfigFormBase {
  /**
     * Config name.
     */
  const SETTINGS = 'entity_template_select.entity_template_setting';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_template_select_setting_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $entities = \Drupal::entityManager()->getEntityTypeLabels(TRUE);

    $form['entities'] = [
      '#type' => 'checkboxes',
      '#title' => t('Entities'),
      '#description' => t('Choose entity use select template function.'),
      '#options' => $entities['Content'],
      '#default_value' => empty($config->get('entities')) ? [] : $config->get('entities'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('entities', $form_state->getValue('entities'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
