Entity Template Selector

---
Adapted for Drupal 8 by Linh Nguyen Manh

Overview:
---------

  Entity Template Select gives content creators a list of templates to choose from as they create nodes. The templates in this list are auto-discovered in your site's default theme through a simple naming convention (node--content-type--[any name]--tp.html.twig).

  The ability to pick templates is used to assign page templates in Wordpress, and this module is a port of similar functionality to Drupal.

  Preparation: Download, install, and enable the module.
  
Setting:
---------

  /admin/config/entity_template_select
  
  Enable Entity being applied 'entity template select'.

Nodes:
------

  1. Create field with field type is 'template select' in content type.
  2. Populate the options list by dropping template files into your theme and naming them appropriately (node--content-type--tp*.html.twig). Here are some examples:

      -  node--article--1--tp.html.twig
      -  node--article--[any-name]--tp.html.twig

  3. When editing a node, pick the template you want from the options list in the vertical tabs. This is the template that will be used to display the node.

Entities:
---------

  With the 1.0 release, you can also pick templates for entities, like users, comments, or taxonomy terms. You can use them in the same way you use node templates. The naming convention for entity templates is entity--bundle--[any-name]--tp.html.twig

  Here are some examples of entity-based template suggestions:

      -  taxonomy-term--tags--[any-name]--tp.html.twig
      -  user--user--[any-name]--tp.html.twig
      -  comment--comment-node-article--[any-name]--tp.html.twig
      -  node--article--[any-name]--tp.html.twig
      -  video--advertisement--[any-name]--tp.html.twig (a custom entity and bundle)

     (note: entity or bundle machine names containing underscores should be converted to hyphens when used in template file names, as shown above)

  You can pick templates for custom entities (which have their own display template), as long as those entities are built off of the Entity API module (https://drupal.org/project/entity).

  Entity Template Select will probably not work with entities built with the ECK module (https://drupal.org/project/eck), until the issues I've reported with ECK form submission (https://drupal.org/node/2163295) have been addressed.


Clean Names:
------------

  Clean names affect how your templates display in the options list like this:

    Display w/out clean name: node--article--aboutus--tp.html.twig
    Display with clean name: Term 1

  You can add a "clean name" to each template by specifying the name in the comments of the template. For example, you can assign the clean name of "Narrow Template" like this:

{#Template Name: Term 1 #}
