<?php

namespace Drupal\entity_template_select\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\Entity;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'template_select_widget' widget.
 *
 * @FieldWidget(
 *   id = "template_select_widget",
 *   label = @Translation("Template select widget"),
 *   field_types = {
 *     "template_select_type"
 *   }
 * )
 */
class TemplateSelectWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $entityAdapter = $items->getParent();
    if ($entityAdapter instanceof EntityAdapter) {
      $entity = $entityAdapter->getValue();
      if ($entity instanceof Entity) {
        $entityType = $entity->getEntityTypeId();
        $current_user = \Drupal::currentUser();
        if ($current_user->hasPermission('allow users to pick ' . $entityType . 'templates')) {
          $bundle = $entity->bundle();
          $options = ['0' => '--Select Template--'];
          $options = array_merge($options, entity_template_select_get_template_options($entityType, $bundle));
          // Applied for node.
          if ($entityType == 'node') {
            $element['template_select'] = $element + [
              '#type' => 'details',
              '#title' => t('Template Select'),
              '#group' => 'advanced',
              '#attributes' => [
                'class' => ['form_template_select'],
              ],
              '#weight' => 1000,
              '#open' => FALSE,
            ];
            $element['template_select']['value'] = [
              '#type' => 'select',
              '#options' => $options,
              '#title' => t('Template Select'),
              '#description' => t('Select template apply for this content'),
              '#default_value' => isset($items[$delta]->value) ?
              $items[$delta]->value : [],
            ];
            return $element;
          }
          $element['value'] = [
            '#type' => 'select',
            '#options' => $options,
            '#title' => t('Template Select'),
            '#description' => t('Select template apply for this content'),
            '#default_value' => isset($items[$delta]->value) ?
            $items[$delta]->value : [],
          ];

        }
      }
    }

    return $element;
  }

}
